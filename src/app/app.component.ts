import { Component } from '@angular/core';
import { CompileShallowModuleMetadata } from '@angular/compiler';

export interface People {
  name: string
  surname: string
  telephone: string
  id?: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'contactsBook';

  search = "";

  people: People[] = [
    {name: "Сергей", surname: "Брин", telephone: "(999) 918-5976", id: 1},
    {name: "Билл", surname: "Гейтс", telephone: "(999) 918-5977", id: 2},
    {name: "Стив", surname: "Джобс", telephone: "(999) 918-5978", id: 3},
    {name: "Павел", surname: "Дуров", telephone: "(999) 918-5979", id: 4},
    {name: "Стив", surname: "Возняк", telephone: "(999) 918-5979", id: 5}
  ]


  updatePeople(p: People) {
    if (this.people.length == 0) {
      p.id = 1;
    } else p.id = this.people[this.people.length-1].id + 1 ;
    this.people.push(p);
  }

  deletePeople(p: People) {
    let index = this.people.findIndex((el)=>el.id==p.id)
    this.people.splice(index, 1);
  }

  savePeople(p: People) {
    let index = this.people.findIndex((el)=>el.id==p.id)
    this.people.splice(index, 1, p);
  }
}
